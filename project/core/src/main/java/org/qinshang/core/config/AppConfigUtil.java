package org.qinshang.core.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @ClassName: AppConfigUtil
 * @Description: 根据键名获取配置文件内的信息
 * @author qdt
 * @date 2018年9月11日 下午5:44:52
 */
public class AppConfigUtil{

	private static Map<String, Object> ctxPropertiesMap = new HashMap<String, Object>();


	public static void loadProperties(Properties properties){
		ctxPropertiesMap.putAll((Map) properties);
		//Map<String, String> map = new HashMap<String, String>((Map) properties);
	}



	/**
	* @Title: getContextProperty
	* @Description:  根据键取值
	* @Param:  String 键
	* @return:  Object
	* @Author: qinshang
	* @Date: 2019/2/24
	*/
	public static Object getContextProperty(String name) {
		return ctxPropertiesMap.get(name);
	}
	/**
	 * @Title: getContextProperty
	 * @Description:  根据键取值
	 * @Param:  String 键
	 * @return:  Object
	 * @Author: qinshang
	 * @Date: 2019/2/24
	 */
	public static Object setContextProperty(String name, Object value) {
		return ctxPropertiesMap.put(name, value);
	}
	/**
	 * @Title: getContextProperty
	 * @Description:  根据键取值
	 * @Param:  []
	 * @return:  Map<String, Object> 获取键值map
	 * @Author: qinshang
	 * @Date: 2019/2/24
	 */
	public static Map<String, Object> getContextPropertyMap() {
		return ctxPropertiesMap;
	}

}