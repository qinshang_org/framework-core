package org.qinshang.core.exception;

import org.qinshang.core.enums.ResultEnum;
import org.springframework.stereotype.Component;


/**
 * @ClassName: BaseException
 * @Description: 通用异常类
 * @author qdt
 * @date 2018年9月11日 下午5:44:11
 */
@Component
public class BaseException extends Exception {

	private Integer code;
	public BaseException() {}
    public BaseException(ResultEnum resultEnum) {
        super(resultEnum.getMsg());
        this.code = resultEnum.getCode();
    }
    public BaseException(Integer code , String msg) {
        super(msg);
        this.code = code;
    }
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
	public ResultEnum getResultEnum() {
		ResultEnum resultEnum = ResultEnum.getResultEnumByCode(code);
		return resultEnum;
	}

}
