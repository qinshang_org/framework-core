package org.qinshang.core.config;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
* @ClassName: DruidDBConfig
* @Description:  DruidDB配置
* @Author: qinshang
* @Date: 2019/2/24
*/
@Configuration
public class DruidDBConfig  {

    private static final Logger druidDBConfigLog = LoggerFactory.getLogger(DruidDBConfig.class);

    /**
    * @Title: startViewServlet
    * @Description:
    * @Param: []
    * @return: org.springframework.boot.web.servlet.ServletRegistrationBean
    * @Author: qinshang
    * @Date: 2019/2/24
    */
    @Bean
    public ServletRegistrationBean startViewServlet(){
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new StatViewServlet(),"/druid/*");
        // IP白名单
        servletRegistrationBean.addInitParameter("allow","127.0.0.1");
        // IP黑名单(共同存在时，deny优先于allow)
        //servletRegistrationBean.addInitParameter("deny","127.0.0.1");
        //控制台管理用户
        servletRegistrationBean.addInitParameter("loginUsername","admin");
        servletRegistrationBean.addInitParameter("loginPassword","123456");
        //是否能够重置数据
        servletRegistrationBean.addInitParameter("resetEnable","false");
        druidDBConfigLog.debug("装配org.springframework.boot.web.servlet.ServletRegistrationBean{}",servletRegistrationBean.hashCode());
        return servletRegistrationBean;
    }


    /**
    * @Title: statFilter
    * @Description:
    * @Param: []
    * @return: org.springframework.boot.web.servlet.FilterRegistrationBean
    * @Author: qinshang
    * @Date: 2019/2/24
    */
    @Bean
    public FilterRegistrationBean statFilter(){
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new WebStatFilter());
        //添加过滤规则
        filterRegistrationBean.addUrlPatterns("/*");
        //忽略过滤的格式
        filterRegistrationBean.addInitParameter("exclusions","*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        druidDBConfigLog.debug("装配org.springframework.boot.web.servlet.FilterRegistrationBean{}",filterRegistrationBean.hashCode());
        return filterRegistrationBean;
    }
}
