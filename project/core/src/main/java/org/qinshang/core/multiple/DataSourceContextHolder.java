package org.qinshang.core.multiple;

/**
* @ClassName: DataSourceContextHolder
* @Description:  设置数据源
* @Author: qinshang
* @Date: 2019/2/24
*/
public class DataSourceContextHolder {

    private static final ThreadLocal<String> CONTEXT_HOLDER = new InheritableThreadLocal<>();

    /**
    * @Title: setDataSource
    * @Description:  设置数据源
    * @Param:  String dbName
    * @return:  void
    * @Author: qinshang
    * @Date: 2019/2/24
    */
    public static void setDataSource(String dbName){
        CONTEXT_HOLDER.set(dbName);
    }

    /**
    * @Title: getDataSource
    * @Description:  取得当前数据源
    * @Param:
    * @return:  String
    * @Author: qinshang
    * @Date: 2019/2/24
    */
    public static String getDataSource(){
        return CONTEXT_HOLDER.get();
    }

    /**
     * 清除上下文数据
     */
    /**
    * @Title: clear
    * @Description:  清除上下文数据
    * @Param:
    * @return:
    * @Author: qinshang
    * @Date: 2019/2/24
    */
    public static void clear(){
        CONTEXT_HOLDER.remove();
    }
}
