package org.qinshang.core.advice;

import org.qinshang.core.enums.ResultEnum;
import org.qinshang.core.exception.BaseException;
import org.qinshang.core.util.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @ClassName: ExceptionHandle
 * @Description: 异常处理
 * @author qdt
 * @date 2018年9月11日 下午5:52:01
 */
@ControllerAdvice
public class ExceptionHandle {
	private static final Logger exceptionHandleLog = LoggerFactory.getLogger(ExceptionHandle.class);
	
	/** 
	 * @Title: handle 
	 * @Description: 处理异常 
	 * @param e
	 * @return Object
	 * @author qdt
	 * @date 2019年1月24日上午10:01:54
	 */ 
	@ExceptionHandler(value = BaseException.class)
	@ResponseBody
    public Object handle(BaseException e) {
		exceptionHandleLog.error("异常信息：",e);
		if (e instanceof BaseException) {
        	BaseException baseException = (BaseException) e;
			return ResultUtil.error(null,baseException.getCode(),baseException.getMessage());
        }else {
            return ResultUtil.error(ResultEnum.UNKONW_ERROR,e.getMessage());
        }
    }
}
