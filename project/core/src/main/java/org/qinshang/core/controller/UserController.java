package org.qinshang.core.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.qinshang.core.entity.User;
import org.qinshang.core.service.UserService;
import org.qinshang.core.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
*
* @author felix-ma
* @create 2019/1/25 14:46
**/
@Api("对User CRUD")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "添加user")
    @PostMapping("/add")
    public Object add(@RequestBody UserVo userVo){
        User user = new User();
        user.setAddress(userVo.getAddress());
        user.setBirthday(new Date());
        user.setSex(userVo.getSex());
        user.setUsername(userVo.getUsername());
        return userService.insert(user)?"添加成功":"添加失败";
    }

    @ApiOperation(value = "添加user")
    @GetMapping("/selectList")
    public Object selectList(){
        Wrapper<User> wrapper = new EntityWrapper<>();
        return userService.selectList(wrapper);
    }

    @GetMapping("/selectListTest")
    public Object selectListTest(){
        return userService.testSelect();
    }

    @ApiOperation(value = "添加user")
    @GetMapping("/selectListdb")
    public Object selectListdb(){
        Wrapper<User> wrapper = new EntityWrapper<>();
        return userService.selectListDB(wrapper);
    }


    @ApiOperation(value = "添加user")
    @GetMapping("/addTransactional")
    public Object addTransactional()throws Exception{
        userService.addTransactional();
        return null;
    }

    @ApiOperation(value = "添加user")
    @PostMapping("/aaaaaaaaaaaa")
    public Object aaaaaaaaaaaa(String name,String Testname){
        System.out.println("name++++++"+name);
        System.out.println("Testname++++++"+Testname);
        return null;
    }

}
