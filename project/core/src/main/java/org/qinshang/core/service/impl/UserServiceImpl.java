package org.qinshang.core.service.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.qinshang.core.annotation.DataSource;
import org.qinshang.core.entity.User;
import org.qinshang.core.mapper.UserMapper;
import org.qinshang.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* 
* @author felix-ma
* @create 2019/1/25 14:51
**/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Override
    @DataSource("db2")
    public boolean insert(User entity) {
        return super.insert(entity);
    }

    @Override
    public boolean deleteById(Serializable id) {
        return super.deleteById(id);
    }

    @Override
    public boolean updateById(User entity) {
        return super.updateById(entity);
    }

    @Override
    public User selectById(Serializable id) {
        return super.selectById(id);
    }

    @Override
    @DataSource("db1")
    public List<User> selectList(Wrapper<User> wrapper) {
        return super.selectList(wrapper);
    }

    @Override
    public List<User> selectListDB(Wrapper<User> wrapper) {
        return super.selectList(wrapper);
    }

    @Override
    public Page<User> selectPage(Page<User> page) {
        return super.selectPage(page);
    }

    @Override
    public Page<User> selectPage(Page<User> page, Wrapper<User> wrapper) {
        return super.selectPage(page, wrapper);
    }
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    @DataSource("db1")
    public void addTransactional()throws Exception{
        User user = new User();
        user.setAddress("111");
        user.setBirthday(new Date());
        user.setSex("1");
        user.setUsername("11");

        this.insert(user);
        throw new RuntimeException();
//        User user2 = new User();
//        user2.setAddress("22");
//        user2.setBirthday(new Date());
//        user2.setSex("2");
//        user2.setUsername("22");
//        this.insert(user2);

    }
    @Autowired
    private UserMapper userMapper;
    @Override
    public List<User> testSelect(){
        return userMapper.testSelect();
    }
}
