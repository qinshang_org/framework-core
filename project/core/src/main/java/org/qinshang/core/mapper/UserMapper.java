package org.qinshang.core.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.qinshang.core.entity.User;

import java.util.List;

/**
* 
* @author felix-ma
* @create 2019/1/25 14:46
**/
public interface UserMapper extends BaseMapper<User> {
    List<User> testSelect ();
}
