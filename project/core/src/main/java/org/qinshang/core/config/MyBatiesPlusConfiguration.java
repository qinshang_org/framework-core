package org.qinshang.core.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.MybatisConfiguration;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.plugins.PerformanceInterceptor;
import com.baomidou.mybatisplus.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.annotation.MapperScan;
import org.qinshang.core.multiple.MultipleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.ResourceUtils;

import javax.sql.DataSource;
import java.io.*;
import java.util.*;

/**
 * @ClassName: MyBatiesPlusConfiguration
 * @Description:  MyBatiesPlus配置
 * @Author: qinshang
 * @Date: 2019/2/24
 */
@Configuration
@MapperScan("org.qinshang.*.mapper*")
////开启事务驱动管理器->启用注解配置事务
@EnableTransactionManagement
public class MyBatiesPlusConfiguration {

    private static final Logger myBatiesPlusConfigurationLog = LoggerFactory.getLogger(MyBatiesPlusConfiguration.class);


    /**
    * @Title: paginationInterceptor
    * @Description:  分页插件，自动识别数据库类型 多租户，请参考官网【插件扩展】
    * @Param: []
    * @return: com.baomidou.mybatisplus.plugins.PaginationInterceptor
    * @Author: qinshang
    * @Date: 2019/2/24
    */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        // 开启 PageHelper 的支持
        paginationInterceptor.setLocalPage(true);
        myBatiesPlusConfigurationLog.debug("装配com.baomidou.mybatisplus.plugins.PaginationInterceptor{}",paginationInterceptor.hashCode());
        return paginationInterceptor;
    }
    /**
     * @Title: performanceInterceptor
     * @Description:  SQL执行效率插件
     * @Param: []
     * @return: com.baomidou.mybatisplus.plugins.PerformanceInterceptor
     * @Author: qinshang
     * @Date: 2019/2/24
     */
    @Bean
    //@Profile({"dev","test"})// 设置 dev test 环境开启
    public PerformanceInterceptor performanceInterceptor() {
        PerformanceInterceptor performanceInterceptor = new PerformanceInterceptor();
        performanceInterceptor.setMaxTime(1000);
        performanceInterceptor.setFormat(true);
        myBatiesPlusConfigurationLog.debug("装配com.baomidou.mybatisplus.plugins.PerformanceInterceptor{}",performanceInterceptor.hashCode());
        return performanceInterceptor;
    }

    @Value("${spring.datasource.max-active:20}")
    Integer maxActive;
    @Value("${spring.datasource.initial-size:1}")
    Integer initialSize;
    @Value("${spring.datasource.max-wait:60000}")
    Integer maxWait;
    @Value("${spring.datasource.min-idle:1}")
    Integer minIdle;
    @Value("${spring.datasource.pool-prepared-statements:true}")
    Boolean poolPreparedStatements;
    @Value("${spring.datasource.time-between-eviction-runs-millis:60000}")
    Integer timeBetweenEvictionRunsMillis;
    @Value("${spring.datasource.max-open-prepared-statements:20}")
    Integer maxOpenPreparedStatements;
    @Value("${spring.datasource.min-evictable-idle-time-millis:300000}")
    Integer minEvictableIdleTimeMillis;
    @Value("${spring.datasource.validation-query:SELECT 1}")
    String validationQuery;
    @Value("${spring.datasource.test-on-borrow:false}")
    Boolean testOnBorrow;
    @Value("${spring.datasource.test-on-return:false}")
    Boolean testOnReturn;
    @Value("${spring.datasource.test-while-idle:true}")
    Boolean testWhileIdle;

    /**
    * @Title: multipleDataSource
    * @Description:  动态数据源配置,借助此方法，开发者只需按一定规则配置数据源的相关链接即可无需重复配置多个数据源
    * @Param: []
    * @return: javax.sql.DataSource
    * @Author: qinshang
    * @Date: 2019/2/24
    */
    @Bean
    @Primary
    public DataSource multipleDataSource() {
        myBatiesPlusConfigurationLog.debug("开始装配动态数据源");
        //AppConfigUtil app = initAppConfigUtil();
        Map<String, Object> ctxPropertiesMap = AppConfigUtil.getContextPropertyMap();//AppConfigUtil.getContextPropertyMap();
        Set<String> keySet = ctxPropertiesMap.keySet();
        Set<String> dataSourceKey = new HashSet<String>();
        for (String key : keySet) {
            if(key.startsWith("jdbc.dataSource")) {
                dataSourceKey.add(key.split("\\.")[2]);
            }
        }
        Map<Object , Object> dataSourcesMap = new HashMap<Object , Object>();


        DruidDataSource dataSource =  null;

        for (String key : dataSourceKey) {
            dataSource =  new DruidDataSource();;
            try{
                // for mysql
                dataSource.setFilters("stat");
            }catch(Exception e){
                e.printStackTrace();
            }

            dataSource.setMaxActive(maxActive);
            dataSource.setInitialSize(initialSize);
            dataSource.setMaxWait(maxWait);
            dataSource.setMinIdle(minIdle);
            dataSource.setPoolPreparedStatements(poolPreparedStatements);
            dataSource.setMaxOpenPreparedStatements(maxOpenPreparedStatements);
            dataSource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
            dataSource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
            dataSource.setValidationQuery(validationQuery);
            dataSource.setTestOnBorrow(testOnBorrow);
            dataSource.setTestOnReturn(testOnReturn);
            dataSource.setTestWhileIdle(testWhileIdle);

            dataSource.setDriverClassName((String)ctxPropertiesMap.get("jdbc.dataSource.#dataSourceName.driverClassName".replace("#dataSourceName", key)));
            dataSource.setUsername((String)ctxPropertiesMap.get("jdbc.dataSource.#dataSourceName.username".replace("#dataSourceName", key)));
            dataSource.setPassword((String)ctxPropertiesMap.get("jdbc.dataSource.#dataSourceName.password".replace("#dataSourceName", key)));
            dataSource.setUrl((String)ctxPropertiesMap.get("jdbc.dataSource.#dataSourceName.url".replace("#dataSourceName", key)));
            dataSource.setInitialSize(Integer.valueOf((String)ctxPropertiesMap.get( "jdbc.dataSource.#dataSourceName.initialSize".replace("#dataSourceName", key))));
            dataSource.setMinIdle(Integer.valueOf((String)ctxPropertiesMap.get( "jdbc.dataSource.#dataSourceName.minIdle".replace("#dataSourceName", key))));
            dataSource.setMaxActive(Integer.valueOf((String)ctxPropertiesMap.get( "jdbc.dataSource.#dataSourceName.maxActive".replace("#dataSourceName", key))));
            dataSourcesMap.put(key, dataSource);
            myBatiesPlusConfigurationLog.debug("{}数据源装配成功",key);
        }
        MultipleDataSource multipleDataSource = new MultipleDataSource();

        //添加数据源
        multipleDataSource.setTargetDataSources(dataSourcesMap);
        //设置默认数据源
        myBatiesPlusConfigurationLog.debug("{}设置为默认数据源",getConfigString("jdbc.default.dataSource"));
        multipleDataSource.setDefaultTargetDataSource(dataSourcesMap.get(getConfigString("jdbc.default.dataSource")));
        myBatiesPlusConfigurationLog.debug("装配javax.sql.DataSource{}",multipleDataSource.hashCode());
        return multipleDataSource;

    }


    /**
     * @Title: sqlSessionFactory
     * @Description: 加载sqlSessionFactory
     * @Param: []
     * @return: org.apache.ibatis.session.SqlSessionFactory
     * @Author: qinshang
     * @Date: 2019/2/24
     */
    @Bean("sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(multipleDataSource());
        sqlSessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:/org/qinshang/core/mapper/*Mapper.xml"));

        MybatisConfiguration configuration = new MybatisConfiguration();
        //configuration.setDefaultScriptingLanguage(MybatisXMLLanguageDriver.class);
        configuration.setJdbcTypeForNull(JdbcType.NULL);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.setCacheEnabled(false);
        sqlSessionFactory.setConfiguration(configuration);
        sqlSessionFactory.setPlugins(new Interceptor[]{ //PerformanceInterceptor(),OptimisticLockerInterceptor()
                paginationInterceptor() //添加分页功能
        });
        //sqlSessionFactory.setGlobalConfig(globalConfiguration());
        myBatiesPlusConfigurationLog.debug("装配jorg.apache.ibatis.session.SqlSessionFactory{}",sqlSessionFactory.getObject().hashCode());
        return sqlSessionFactory.getObject();
    }

    /**
    * @Title: txManager
    * @Description: 创建事务管理器1
    * @param []
    * @return org.springframework.transaction.PlatformTransactionManager
    * @author qinshang
    * @date 2019/2/26
    */
    @Bean
    public PlatformTransactionManager txManager() {
        return new DataSourceTransactionManager(multipleDataSource());
    }

    /*@Bean
    public GlobalConfiguration globalConfiguration() {
        GlobalConfiguration conf = new GlobalConfiguration(new LogicSqlInjector());
        conf.setLogicDeleteValue("-1");
        conf.setLogicNotDeleteValue("1");
        conf.setIdType(0);
        //conf.setMetaObjectHandler(new MyMetaObjectHandler());
        conf.setDbColumnUnderline(true);
        conf.setRefresh(true);
        return conf;
    }*/

    @Autowired
    AppConfigUtil appConfigUtil;

    /**
    * @Title: initAppConfigUtil
    * @Description:  配置文件读取工具类
    * @Param: []
    * @return: org.qinshang.core.config.AppConfigUtil
    * @Author: qinshang
    * @Date: 2019/2/24
    */
    @Bean(name = "appConfigUtil")
    public  AppConfigUtil initAppConfigUtil(){
        AppConfigUtil initAppConfigUtil = new AppConfigUtil();
        BufferedReader bufferedReader = null;
        try{
            Properties properties = new Properties();
            bufferedReader = new BufferedReader(new FileReader(ResourceUtils.getURL("classpath:").getPath()+"jdbc.properties"));
            properties.load(bufferedReader);
            AppConfigUtil.loadProperties(properties);
        }catch (Exception e) {
            myBatiesPlusConfigurationLog.error("装配org.qinshang.core.config.AppConfigUtil资源失败，{}",e.getMessage());
        }finally {
            myBatiesPlusConfigurationLog.error("关闭资源文件 jdbc.properties");
            if (bufferedReader != null){
                try{
                    bufferedReader.close();
                }catch (Exception e){
                    myBatiesPlusConfigurationLog.error("关闭资源文件 jdbc.properties 失败");
                }
            }
        }
        return initAppConfigUtil;
    }
    private String getConfigString(String name){
        return (String)AppConfigUtil.getContextProperty(name);
    }

}
