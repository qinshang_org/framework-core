package org.qinshang.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author qinshang
 * @ClassName: WebConfig
 * @Description: 解决跨域
 * @date 2019/02/25
 */
@Configuration
public class WebConfig {

    @Bean
    public ResponseBodyWrapFactoryBean getResponseBodyWrap(){

        return new ResponseBodyWrapFactoryBean();

    }

    /**
    * @Title: corsConfigurer
    * @Description:  解决跨域
    * @Param: []
    * @return: org.springframework.web.servlet.config.annotation.WebMvcConfigurer
    * @Author: qinshang
    * @Date: 2019/2/25
    */
     @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowCredentials(true)
                        .allowedMethods("GET", "POST", "DELETE", "PUT","PATCH")
                        .maxAge(3600);
            }
        };
    }

}
