package org.qinshang.core.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import org.qinshang.core.entity.User;

import java.util.List;


/**
 *
 * @author felix-ma
 * @create 2019/1/25 14:46
 **/
public interface UserService extends IService<User> {


    public List<User> selectListDB(Wrapper<User> wrapper);
    public List<User> testSelect();

    public void addTransactional()throws Exception;

}
