package org.qinshang.core.multiple;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
* @ClassName: MultipleDataSource
* @Description:
* @Author: qinshang
* @Date: 2019/2/24
*/
public class MultipleDataSource extends AbstractRoutingDataSource {

    /**
    * @Title: determineCurrentLookupKey
    * @Description:
    * @Param: []
    * @return: java.lang.Object
    * @Author: qinshang
    * @Date: 2019/2/24
    */
    @Override
    protected Object determineCurrentLookupKey() {
        return DataSourceContextHolder.getDataSource();
    }
}
