package org.qinshang.core.config;
 
import org.qinshang.core.entity.Result;
import org.qinshang.core.util.ResultUtil;
import org.springframework.core.MethodParameter;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;
/**
 * @author suntongxin
 * Create on 2017年12月12日上午10:48:54
 * All right reserved
 */
public class ResponseBodyWrapHandler implements HandlerMethodReturnValueHandler {

	private final HandlerMethodReturnValueHandler delegate;

	public ResponseBodyWrapHandler(HandlerMethodReturnValueHandler delegate) {

		this.delegate = delegate;

	}

	@Override
	public boolean supportsReturnType(MethodParameter returnType) {

		return delegate.supportsReturnType(returnType);

	}

	@Override
	public void handleReturnValue(Object returnValue, MethodParameter returnType, ModelAndViewContainer mavContainer,
								  NativeWebRequest webRequest) throws Exception {
		Result result = null;
		if (returnValue instanceof Result) {
			result = (Result) returnValue;
		} else {
			result = ResultUtil.success(returnValue);
		}

		delegate.handleReturnValue(result, returnType, mavContainer, webRequest);
	}

}