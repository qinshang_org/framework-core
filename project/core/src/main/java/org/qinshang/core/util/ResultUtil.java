package org.qinshang.core.util;

import java.util.Collection;

import org.qinshang.core.entity.Result;
import org.qinshang.core.enums.ResultEnum;
import org.springframework.stereotype.Component;

/**
 * @ClassName: ResultUtil
 * @Description: 格式化处理后端返回给前端的数据
 * @author qdt
 * @date 2018年9月11日 下午5:42:08
 */
@Component
public class ResultUtil {
	public static <E> Result<E> success(E data, Integer code, String msg) {
		return result(data, code, msg);
	}

	public static <E> Result<E> success(E data, ResultEnum resultEnum) {
		resultEnum = resultEnum == null ? ResultEnum.SUCCESS : resultEnum ;
		return success(data, resultEnum.getCode(), resultEnum.getMsg());
	}

	public static <E> Result<E> success(E data) {
		return success(data, ResultEnum.SUCCESS);
	}

	public static Result success() {
		return success(ResultEnum.SUCCESS);
	}

	public static Result success(ResultEnum resultEnum) {
		resultEnum = resultEnum == null ? ResultEnum.SUCCESS : resultEnum ;
		return success(null, resultEnum );
	}

	public static <E> Result<E> result(E data, Integer code, String msg) {
		Result<E> result = new Result<E>();
		result.setCode(code);
		result.setMsg(msg);
		result.setData(data);
		if (data instanceof Collection){
			result.setRecordsTotal(((Collection) data).size());
		}
		else{
			result.setRecordsTotal(1);
		}

		return result;
	}
	public static <E> Result<E> result(E data, Integer code, String msg, String systemMSG) {
		Result<E> result = new Result<E>();
		result.setCode(code);
		result.setMsg(msg);
		result.setData(data);
		result.setSystemMSG(systemMSG);
		if (data instanceof Collection){
			result.setRecordsTotal(((Collection) data).size());
		}
		else{
			result.setRecordsTotal(1);
		}
		return result;
	}



	public static <E> Result<E> error(E data, Integer code, String msg) {
		return result(data, code, msg);
	}
	public static <E> Result<E> error(E data, Integer code, String msg, String systemMSG) {
		return result(data, code, msg,systemMSG);
	}
	public static <E> Result<E> error(E data, ResultEnum resultEnum) {
		resultEnum = resultEnum == null ? ResultEnum.NOT_FOND_ERROR : resultEnum ;
		return error(data, resultEnum.getCode(), resultEnum.getMsg());
	}

	public static <E> Result<E> error(E data) {
		return error(data, ResultEnum.UNKONW_ERROR);
	}

	public static Result error() {
		return error(ResultEnum.NOT_FOND_ERROR);
	}

	public static Result error(ResultEnum resultEnum) {
		resultEnum = resultEnum == null ? ResultEnum.NOT_FOND_ERROR : resultEnum ;
		return error(null, resultEnum);
	}
	public static Result error(ResultEnum resultEnum, String systemMSG) {
		resultEnum = resultEnum == null ? ResultEnum.NOT_FOND_ERROR : resultEnum ;
		return error(null, resultEnum, systemMSG);
	}
	public static <E> Result<E> error(E data, ResultEnum resultEnum, String systemMSG) {
		resultEnum = resultEnum == null ? ResultEnum.NOT_FOND_ERROR : resultEnum ;
		return error(data, resultEnum.getCode(), resultEnum.getMsg(),systemMSG);
	}
}
