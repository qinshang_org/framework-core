package org.qinshang.core.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.qinshang.core.annotation.DataSource;
import org.qinshang.core.multiple.DataSourceContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


@Component
@Slf4j
@Aspect
@Order(-1)
/**
* @ClassName: DataSourceAspect
* @Description:  数据源切换切面
* @Author: qinshang
* @Date: 2019/2/24
*/
public class DataSourceAspect {

    private static final Logger dataSourceAspectLog = LoggerFactory.getLogger(DataSourceAspect.class);

    /**
     * @Title: pointCut
     * @Description: 切面，以 DataSource 注解为切面切入条件
     * @Param: []
     * @return: void
     * @Author: qinshang
     * @Date: 2019/2/24
     */
    @Pointcut("@within(org.qinshang.core.annotation.DataSource) || @annotation(org.qinshang.core.annotation.DataSource)")//    @Pointcut("@within(org.qinshang.core.annotation.DataSource)")
    public void pointCut(){ }

    /**
    * @Title: doBefore
    * @Description:  获取由@DataSource指定的数据源标识，设置到线程存储中以便切换数据源
    * @Param: [dataSource]
    * @return: void
    * @Author: qinshang
    * @Date: 2019/2/24
    */
    @Before("pointCut() && @annotation(dataSource)")
    public void doBefore(DataSource dataSource){
        dataSourceAspectLog.debug("切换后的数据源{}", dataSource.value());
        DataSourceContextHolder.setDataSource(dataSource.value());
    }

    /**
     * @Title: doAfter
     * @Description: 还原
     * @Param: []
     * @return: void
     * @Author: qinshang
     * @Date: 2019/2/24
     */
    @After("pointCut()")
    public void doAfter(){
        dataSourceAspectLog.debug("回默认数据源{}");
        DataSourceContextHolder.clear();
    }
}
