package org.qinshang.core.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @ClassName: SwaggerConfiguration
 * @Description:  Swagger配置文件
 * @Author: qinshang
 * @Date: 2019/2/24
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    private static final Logger SwaggerConfigurationLog = LoggerFactory.getLogger(SwaggerConfiguration.class);

    /**
    * @Title: createRestApi
    * @Description:
    * @Param: []
    * @return: springfox.documentation.spring.web.plugins.Docket
    * @Author: qinshang
    * @Date: 2019/2/24
    */
    @Bean
    public Docket createRestApi() {
        SwaggerConfigurationLog.debug("装配springfox.documentation.spring.web.plugins.Docket");
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.qinshang.*.controller*"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("多数据源 SSM 测试服务")
                .description("多数据源 SSM 测试文档")
                .termsOfServiceUrl("https://qinshang-blog.cf")
                .version("1.0")
                .build();
    }

}
