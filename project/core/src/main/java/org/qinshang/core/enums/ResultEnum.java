package org.qinshang.core.enums;

/**
 * @ClassName: ResultEnum
 * @Description: 系统操作代码及对应信息
 * @author qdt
 * @date 2018年9月11日 下午5:43:27
 */
public enum ResultEnum {
    /**
    * @Title:  ResultEnum.UNKONW_ERROR
    * @Description: 未知错误
    * @author qinshang
    * @date 2019/3/8
    */
    UNKONW_ERROR(-1, "鉴权失败"),
    /**
     * @Title:  ResultEnum.SUCCESS
     * @Description: 成功
     * @author qinshang
     * @date 2019/3/8
     */
    SUCCESS(200, "成功"),
    /**
     * @Title:  ResultEnum.NOT_FOND_ERROR
     * @Description: 请求资源未找到
     * @author qinshang
     * @date 2019/3/8
     */
    NOT_FOND_ERROR(404, "请求资源未找到"),
    /**
     * @Title:  ResultEnum.SYSTEM_ERROR
     * @Description: 系统内部错误
     * @author qinshang
     * @date 2019/3/8
     */
    SYSTEM_ERROR(500, "系统内部错误")
    ;

    private Integer code;

    private String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
    /**
     * @Title: getResultEnumByCode 
     * @Description: 根据code获取对应枚举类型 
     * @param code
     * @return ResultEnum
     * @author qdt
     * @date 2019年1月24日上午9:12:25
     */ 
    public static ResultEnum getResultEnumByCode(Integer code){
    	for (ResultEnum resultEnum : ResultEnum.values() ) {
			if(resultEnum.getCode().equals(code)){
                return resultEnum;
            }
		}
    	return null;
    }
}