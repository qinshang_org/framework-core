package org.qinshang.core.aop;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.qinshang.core.entity.Result;
import org.qinshang.core.util.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSONObject;
/**
 * @ClassName: HttpAspect
 * @Description: Controller切面
 * @author qdt
 * @date 2018年9月14日 下午1:42:08
 */
@Aspect
@Component
public class HttpAspect {
	private static final Logger httpAspectLog = LoggerFactory.getLogger(HttpAspect.class);

	@Pointcut("@within(org.springframework.web.bind.annotation.RequestMapping)|| @annotation(org.springframework.web.bind.annotation.RequestMapping)")//@Pointcut("@within(org.springframework.web.bind.annotation.RequestMapping)")
	public void controllerPoint() {}
	
	
	/** 
	 * @Title: doBefore 
	 * @Description: 打印接口日志 
	 * @param joinPoint void
	 * @author qdt
	 * @date 2019年1月24日上午10:03:42
	 */ 
	@Before("controllerPoint()")
	public void doBefore(JoinPoint joinPoint) {
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes();
		HttpServletRequest request = attributes.getRequest();
		httpAspectLog.info("开始处理请求");
		// url
		httpAspectLog.debug("地址url={}" , request.getRequestURL());
		// method
		httpAspectLog.debug("请求方式method={}" , request.getMethod());
		// ip
		httpAspectLog.debug("IP地址ip={}" , request.getRemoteAddr());
		// 类方法
		httpAspectLog.debug("访问接口方法class_method={}",
				joinPoint.getSignature().getDeclaringTypeName() + "."
				+ joinPoint.getSignature().getName());
		// 参数
		Object objArgs = joinPoint.getArgs()==null?joinPoint.getArgs():new Object();
		httpAspectLog.debug("传入参数args={}" , JSONObject.toJSONString(objArgs));
	}
//	/**
//	 * @Title: doResult
//	 * @Description: 处理回馈给前台的数据
//	 * @param pjp
//	 * @return Object
//	 * @throws Throwable
//	 * @author qdt
//	 * @date 2018年10月8日下午4:49:44
//	 */
	@Around("controllerPoint()")
	public Object doResult(ProceedingJoinPoint pjp) throws Throwable {
		httpAspectLog.info("返回请求所需数据");
		// 执行该方法
		Object object = pjp.proceed();
		if (object instanceof Result){
			return object;
		}
		return object;//ResultUtil.success(object);
	}

	 @AfterReturning(returning = "object", pointcut = "controllerPoint()")
	 public void doAfterReturning(Object object) {
		httpAspectLog.debug("返回参数return={}",object);
	 }

}
