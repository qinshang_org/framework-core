package org.qinshang;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoreApplication {
//    @Value("${a.b}")
//    String aaa;
    public static void main(String[] args) {
        SpringApplication.run(CoreApplication.class, args);
        System.out.printf(
                "\n----------------------------------------------------------\n\t"
                        + "Application is running! Access URLs:\n\t"
                        + "Local: \t\thttp://localhost:8080\n\t"
                        + "\n----------------------------------------------------------\n"
        );
    }

}
