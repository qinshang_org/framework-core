package org.qinshang.core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.qinshang.core.entity.User;
import org.qinshang.core.mapper.UserMapper;
import org.qinshang.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CoreApplicationTests {
    //@Autowired
    //private UserMapper userMapper;
    @Autowired
    private UserService userMapper;
    @Test
    public void contextLoads() {
        List<User> users = userMapper.testSelect();
        System.out.println(users.size());
    }

}
